FROM ubuntu:14.04
MAINTAINER Yiwei Chen <yiwei.chen@pearson.com>
# Ensure there are enough file descriptors for running Fluentd
# need to be latger
RUN ulimit -n 65536
# Disable prompts from apt.
ENV DEBIAN_FRONTEND noninteractive
# Install prerequisites.
RUN apt-get update && \
apt-get install -y -q curl make g++ git && \
apt-get clean && \
rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
# Install Fluentd
RUN /usr/bin/curl -L https://td-toolbelt.herokuapp.com/sh/install-ubuntu-trusty-td-agent2.sh | sh
# Change the default user and group to root.
# Needed to allow access to /var/log/docker/... files.
RUN sed -i -e "s/USER=td-agent/USER=root/" -e "s/GROUP=td-agent/GROUP=root/" /etc/init.d/td-agent
# Install the Elasticsearch Fluentd plug-in.
RUN td-agent-gem install fluent-plugin-kubernetes_metadata_filter fluent-plugin-elasticsearch
# Clone Fluentd Kafka plugin from github
RUN git clone https://github.com/htgc/fluent-plugin-kafka.git
# Install Kafka plugin
RUN td-agent-gem install fluent-plugin-kafka fluent-plugin-kubernetes fluent-plugin-kinesis
# Install the copy plugin
RUN td-agent-gem install fluent-plugin-copy_ex
# Copy the Fluentd configuration file.
COPY td-agent.conf /etc/td-agent/td-agent.conf
# Run the Fluentd service.
ENTRYPOINT ["td-agent"]

